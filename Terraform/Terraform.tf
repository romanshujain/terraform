provider "aws" {
  region     = "us-east-2"
}

resource "aws_ecs_task_definition" "kibana" {
  family = "kibana"
  container_definitions = "${file("task-definitions/service.json")}"

}

